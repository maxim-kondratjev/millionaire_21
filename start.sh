#!/usr/bin/env bash

#./load_models.sh

# prepare image
docker build -t bootcamp-millionaire-5:latest .

# run container
docker stop millionaire-5 || echo "first creation"
docker rm millionaire-5 || echo "first creation"
docker run -d -p 12300:12300 --name millionaire-5 bootcamp-millionaire-5