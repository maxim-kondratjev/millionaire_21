#!/usr/bin/env bash

mkdir models
cd models

curl https://transfer.sh/eGoTKf/nn_index.bin --output nn_index.bin
curl https://transfer.sh/4pqIvY/tf_idf.pickle --output tf_idf.pickle
curl https://transfer.sh/S8cXSK/PCA.pickle --output PCA.pickle
