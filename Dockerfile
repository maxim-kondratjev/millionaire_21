FROM python:3.9-slim

WORKDIR /app
COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . /app
RUN mv models data

EXPOSE 12300
ENV PYTHONPATH="/app:/app/src"

CMD ["python", "src/app.py"]