import typing
from pathlib import Path

import numpy as np
from loguru import logger
from navec import Navec
from scipy import sparse
from scipy.sparse import csr
from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm, trange
from multiprocessing import Pool

from src.constants import DATA_DIR
from src.tf_idf import load_tf_idf


def get_words_weghts(X_tf_idf: csr, feature_names: list[str]) -> typing.Generator[tuple[list[str], list[float]], None, None]:
    i = 0
    rows, cols = X_tf_idf.nonzero()
    n_elements = rows.shape[0]
    with tqdm(total=n_elements) as pbar:
        while i < n_elements:
            cur_row = rows[i]
            j = i

            # find all words for cur text
            words: list[str] = []
            weights: list[float] = []
            while j < n_elements and cur_row == rows[j]:
                words.append(feature_names[cols[j]])
                weights.append(X_tf_idf[cur_row, cols[j]])
                j += 1

            yield words, weights

            # next step
            pbar.update(j - i)
            i = j


def text_vector(words: list[str], tf_weights: list[float], navec: Navec) -> np.ndarray:
    vectors: list[np.ndarray] = [navec.get(word, navec['<unk>']) for word in words]
    np_vectors = np.array(vectors).reshape(-1, 300)
    vector = np.dot(np.array(tf_weights), np_vectors)
    return vector


def tf2embedding(X_tf_idf: csr, tf_idf: TfidfVectorizer, navec: Navec) -> np.ndarray:
    feature_names: list[str] = tf_idf.get_feature_names()

    def _func(words_weights_pair: tuple[list[str], list[float]]) -> np.ndarray:
        return text_vector(words=words_weights_pair[0], tf_weights=words_weights_pair[1], navec=navec)

    with Pool(10) as pool:
        vectors: list[np.ndarray] = pool.map(_func, get_words_weghts(X_tf_idf=X_tf_idf, feature_names=feature_names))

    X_embed = np.array(vectors).reshape(X_tf_idf.shape[0], 300)

    return X_embed


if __name__ == "__main__":
    logger.debug(f"Load X Tf-Idf")
    X_tf_idf_path: Path = DATA_DIR / "X_tf_idf.npz"
    X_tf_idf: csr = sparse.load_npz(X_tf_idf_path)

    logger.debug(f"Load Tf-Idf")
    tf_idf_path: Path = DATA_DIR / "tf_idf.pickle"
    tf_idf: TfidfVectorizer = load_tf_idf(path=tf_idf_path)

    logger.debug(f"Load navec")
    filename_navec: Path = DATA_DIR / "navec_hudlit_v1_12B_500K_300d_100q.tar"
    navec = Navec.load(filename_navec)

    logger.debug(f"Eval X_embed")
    X_embed = tf2embedding(X_tf_idf=X_tf_idf, tf_idf=tf_idf, navec=navec)

    logger.debug(f"Save X_embed")
    X_embed_path: Path = DATA_DIR / "X_embed.npy"
    with X_embed_path.open('wb') as file:
        np.save(file, X_embed)
    logger.debug(f"X_embed saved to: {X_embed_path}")

    a = 1
