from pathlib import Path

import nmslib
import numpy as np
from loguru import logger

import typing
from nmslib.dist import FloatIndex
from scipy import sparse
from scipy.sparse import csr

from src.constants import DATA_DIR

from sklearn.decomposition import TruncatedSVD
import pickle



def create_nn_index(X_tf_idf: csr, index_path: Path, index_params: dict):
    # initialize a new index, using a HNSW index on Cosine Similarity and save
    index: FloatIndex = nmslib.init(method='hnsw', space='cosinesimil', data_type=nmslib.DataType.DENSE_VECTOR)
    index.addDataPointBatch(X_tf_idf)
    index.createIndex(index_params, print_progress=True)
    index.saveIndex(str(index_path), save_data=False)


def get_num_of_answer(index: FloatIndex, vectors) -> int:
    # get all nearest neighbours for all the datapoint
    # using a pool of 4 threads to compute
    neighbours = index.knnQueryBatch(vectors, k=1, num_threads=4)
    num_answer: int = np.argmin([distances[0] for ids, distances in neighbours]) + 1  # 1-4
    return num_answer

def get_answers(index: FloatIndex, vectors) -> typing.List[float]:
    neighbours = index.knnQueryBatch(vectors, k=1, num_threads=4)
    return [1/distances[0] for ids, distances in neighbours]

if __name__ == "__main__":
    M = 15
    efC = 100
    num_threads = 4
    index_params = {'M': M, 'indexThreadQty': num_threads, 'efConstruction': efC, 'post': 0}
    index_path: Path = DATA_DIR / "nn_index.bin"
    pca_path: Path = DATA_DIR/ "PCA.pickle"

    # create a random matrix to index
    logger.debug(f"Load X Tf-Idf")
    X_tf_idf_path: Path = DATA_DIR / "X_tf_idf.npz"
    X_tf_idf_sparse: csr = sparse.load_npz(X_tf_idf_path)

    clf = TruncatedSVD(100)
    X_tf_idf_pca = clf.fit_transform(X_tf_idf_sparse)
    print(clf.explained_variance_ratio_.sum())
    with pca_path.open("wb") as file:
        pickle.dump(clf, file)

    create_nn_index(X_tf_idf=X_tf_idf_pca, index_path=index_path, index_params=index_params)

    # load
    index: FloatIndex = nmslib.init(method='hnsw', space='cosinesimil', data_type=nmslib.DataType.DENSE_VECTOR)
    index.loadIndex(str(index_path))

    # predict
    vectors = X_tf_idf_pca[:2]
    answers_score = get_answers(index=index, vectors=vectors)

