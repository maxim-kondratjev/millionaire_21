from numpy.random import choice
from scipy.special import softmax
import itertools
import numpy as np



def decision(answer_scores, bank_money, saved_money, question_number, available_hints, money, num_a): #принятие решения. возвращаем индекс ответа по списку скоров или название подсказки или забрать деньги
    answer_scores_softmax = softmax(answer_scores)
    if num_a is not None:
        answer_scores_softmax[num_a - 1] = 0

    potential_bank = money[question_number][0]
    risk = list(map(lambda ass: ass*(potential_bank-bank_money)-(1-ass)*(bank_money-saved_money), answer_scores_softmax))
    #оценка риска при подсказказах
    #50:50
    fifty_fifty_risk = [0]
    if 'fifty fifty' in available_hints:
        possible_combinations = list(itertools.combinations(answer_scores, 2))
        mean_soft_comb = softmax([np.mean([x[0] for x in possible_combinations]), np.mean([x[1] for x in possible_combinations])])
        fifty_fifty_risk = list(map(lambda ass: ass*(potential_bank-bank_money)-(1-ass)*(bank_money-saved_money), mean_soft_comb))
    #возможность ошибиться
    soft_risk = [0]
    if 'can mistake' in available_hints:
        mistake = answer_scores.pop(answer_scores.index(max(answer_scores)))
        soft_miss = softmax(answer_scores)
        soft_risk = list(map(lambda ass: ass*(potential_bank-bank_money)-(1-ass)*(bank_money-saved_money), soft_miss))

    #применять ли подсказку из оценки достижения несгораемой суммы с учетом равномерного распределения вероятностей ответов
    prob_scale = (1+pow(0.25, money[question_number:].index(next(filter(lambda x: x[1]==True, money[question_number:])))))


    if max(risk)>0:
        return 'answer', risk.index(max(risk))+1
    elif max(fifty_fifty_risk)*prob_scale>max(risk) and max(fifty_fifty_risk)>0 and ('fifty fifty' in available_hints):
        return 'fifty fifty', None
    elif max(soft_risk)*prob_scale>max(risk) and max(soft_risk)>0 and ('can mistake' in available_hints):
        return 'can mistake', risk.index(max(risk))+1
    elif 'new question' in available_hints:
        return 'new question', None
    elif available_hints:
        if soft_risk>fifty_fifty_risk and 'can mistake' in available_hints and 'fifty fifty' in available_hints:
            return 'can mistake', risk.index(max(risk))+1
        elif soft_risk<=fifty_fifty_risk and 'can mistake' in available_hints and 'fifty fifty' in available_hints:
            return 'fifty fifty', None
        elif 'can mistake' in available_hints:
            return 'can mistake', risk.index(max(risk))+1
        elif 'fifty fifty' in available_hints:
            return 'fifty fifty', None
    return 'take money', None


