import typing
from enum import Enum

import pydantic
from pydantic import Extra


class HelpOptions(str, Enum):
    FIFTY_FIFTY = "fifty fifty"
    CAN_MISTAKE = "can mistake"
    NEW_QUESTION = "new question"


class EndGameOptions(str, Enum):
    TAKE_MONEY = "take money"


class ResponseType(str, Enum):
    GOOD = "good"
    BAD = "bad"
    TRY_AGAIN = "try again"

class PredictInputModel(pydantic.BaseModel):
    """
    data:
    {
        "number of game": 5,

        "question": "Что есть у Пескова?",
        "answer_1": "Усы",
        "answer_2": "Борода",
        "answer_3": "Лысина",
        "answer_4": "Третья нога",

        "question money": 4000,
        "saved money": 1000,
        "available help": ["fifty fifty", "can mistake", "new question"]
    }
    """
    number_of_game: int = pydantic.Field(alias="number of game")
    question: str
    answer_1: typing.Optional[str]
    answer_2: typing.Optional[str]
    answer_3: typing.Optional[str]
    answer_4: typing.Optional[str]
    question_money: int = pydantic.Field(alias="question money")
    saved_money: int = pydantic.Field(alias="saved money")
    available_help: list[HelpOptions] = pydantic.Field([], alias="available help")

    @property
    def answers_list(self) -> list[typing.Optional[str]]:
        return [self.answer_1, self.answer_2, self.answer_3, self.answer_4]

    class Config:
        extra = Extra.allow


class FullPredictInputModel(pydantic.BaseModel):
    data: PredictInputModel

    class Config:
        extra = Extra.allow


class PredictAnswerModel(pydantic.BaseModel):
    """
    resp:
    {
        "help": "fifty fifty",
    }

    resp:
    {
        "help": "can mistake",
        "answer": 1,
    }

    resp:
    {
        "end game": "take money",
    }
    """
    answer: typing.Optional[int]
    help: typing.Optional[HelpOptions]
    end_game: typing.Optional[EndGameOptions] = pydantic.Field(alias="end game")

    class Config:
        extra = Extra.allow


class ResultQuestionInputModel(pydantic.BaseModel):
    """
    data:
    {
        "number of game": 5,

        "question": "Что есть у Пескова?",
        "answer": 1,

        "bank": 4000,
        "saved money": 1000,
        "response type": "good"
    }

    data:
    {
        "number of game": 5,

        "question": "Что есть у Пескова?",
        "answer": 4,

        "bank": 1000,
        "saved money": 1000,
        "response type": "bad"
    }
    """
    number_of_game: int = pydantic.Field(alias="number of game")
    question: str
    answer: int
    bank: int
    saved_money: int = pydantic.Field(alias="saved money")
    response_type: ResponseType = pydantic.Field(alias="response type")

    class Config:
        extra = Extra.allow
