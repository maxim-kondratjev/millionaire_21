import pickle
import string
import typing
from pathlib import Path

# import pymorphy2
from scipy import sparse
from scipy.sparse import csr
from loguru import logger
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.tokenize import word_tokenize

from src.constants import DATA_DIR
from src.parse_wiki_articles import wiki_articles_generator


# class Preprocessor:
#     def __init__(self):
#         self._table = str.maketrans('', '', string.punctuation)
#         self._morph = pymorphy2.MorphAnalyzer()
#
#     def __call__(self, text: str) -> str:
#         tokens = word_tokenize(text)
#
#         words: list[str] = [
#             self._morph.parse(word.translate(self._table))[0].normal_form
#             for word in tokens
#             if word.isalpha()
#         ]
#         return ' '.join(words)


def create_wiki_tf_idf(wiki_texts_dir: Path, **tf_idf_kwargs):
    logger.debug("Read wiki")
    texts: list[str] = list(wiki_articles_generator(wiki_texts_dir=wiki_texts_dir))

    logger.debug("Fit Tf-Idf")
    tf_idf: TfidfVectorizer = TfidfVectorizer(**tf_idf_kwargs)
    X: csr = tf_idf.fit_transform(raw_documents=texts)
    logger.debug("Tf-Idf fitted")

    return tf_idf, X


def load_tf_idf(path: Path) -> TfidfVectorizer:
    with path.open("rb") as file:
        return pickle.load(file)


if __name__ == '__main__':
    wiki_texts_dir: Path = DATA_DIR / "text"

    # preprocessor: Preprocessor = Preprocessor()
    tf_idf_kwargs: dict = {
        # 'preprocessor': preprocessor,
    }
    tf_idf, X_tf_idf = create_wiki_tf_idf(wiki_texts_dir=wiki_texts_dir, **tf_idf_kwargs)

    tf_idf_path: Path = DATA_DIR / "tf_idf.pickle"
    with tf_idf_path.open("wb") as file:
        pickle.dump(tf_idf, file)
    logger.debug(f"Tf-Idf saved to {tf_idf_path}")

    X_tf_idf_path: Path = DATA_DIR / "X_tf_idf.npz"
    sparse.save_npz(X_tf_idf_path, X_tf_idf)
    logger.debug(f"X Tf-Idf saved to {X_tf_idf_path}")
