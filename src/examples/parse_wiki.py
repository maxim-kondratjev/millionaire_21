from pathlib import Path

from src.constants import DATA_DIR
from src.parse_wiki_articles import wiki_articles_generator

if __name__ == "__main__":
    wiki_texts_dir: Path = DATA_DIR / "text"
    for article in wiki_articles_generator(wiki_texts_dir=wiki_texts_dir):
        print(article)
        print("="*100)
