import requests

server_host = "http://127.0.0.1:12300"

if __name__ == "__main__":
    # request_data = {
    #     "number of game": 5,
    #
    #     "question": "Что есть у Пескова?",
    #     "answer_1": "Борода",
    #     "answer_2": "Усы",
    #     "answer_3": "Лысина",
    #     "answer_4": "Третья нога",
    #
    #     "question money": 4000,
    #     "saved money": 1000,
    #     "available help": ["fifty fifty", "can mistake", "new question"]
    # }
    # tmp = requests.post(
    #     f'{server_host}/predict',
    #     data=request_data)


    request_data = {
        "number of game": 5,

        "question": "Что есть у Пескова?",
        "answer": 4,

        "bank": 1000,
        "saved money": 1000,
        "response type": "bad"
    }
    tmp = requests.post(
        f'{server_host}/result_question',
        data=request_data)
    print(tmp.json())