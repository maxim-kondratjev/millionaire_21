#!/usr/bin/env bash

FILENAME="urls.txt"
rm $FILENAME
touch $FILENAME

curl --upload-file data/nn_index.bin https://transfer.sh/nn_index.bin >> $FILENAME
echo "" >> $FILENAME

curl --upload-file data/tf_idf.pickle https://transfer.sh/tf_idf.pickle >> $FILENAME
echo "" >> $FILENAME

curl --upload-file data/PCA.pickle https://transfer.sh/PCA.pickle >> $FILENAME
echo "" >> $FILENAME
